<?php

namespace Tordo\AFIP\Services;

class Voucher
{
    /**
    * The voucher number.
    *
    * @var integer
    */
    private $voucherNumber;


    private $texempt;

    /**
    * The voucher type.
    *
    * @var integer
    */
    private $voucherType;

    /**
    * The point sale.
    *
    * @var integer
    */
    private $pointSale;

    /**
    * The document type.
    *
    * @var integer
    */
    private $documentType;

    /**
    * The document number.
    *
    * @var integer
    */
    private $documentNumber;

    /**
    * date.
    *
    * @var string
    */
    private $date;

    /**
    * The currency type.
    *
    * @var string
    */
    private $currencyType;

    /**
    * The currency trading.
    *
    * @var integer
    */
    private $currencyTrading;

    /**
    * from date.
    *
    * @var string
    */
    private $fromDate;

    /**
    * to date.
    *
    * @var string
    */
    private $toDate;

    /**
    * expiration date.
    *
    * @var string
    */
    private $expirationDate;



    private $taxes = [];

    /**
    * Set or Get voucher number.
    *
    * @return object current instance.
    */
    public function voucherNumber($voucherNumber = null)
    {
        if (is_null($voucherNumber)) {
            return $this->voucherNumber;
        }

        $this->voucherNumber = $voucherNumber;
        return $this;
    }

    /**
    * Set or Get voucher type.
    *
    * @return object current instance.
    */
    public function voucherType($voucherType = null)
    {
        if (is_null($voucherType)) {
            return $this->voucherType;
        }

        $this->voucherType = $voucherType;
        return $this;
    }

    /**
    * Set or Get point sale.
    *
    * @return object current instance.
    */
    public function totalExempt($texempt = null)
    {
        if (is_null($texempt)) {
            return $this->texempt;
        }

        $this->texempt = $texempt;
        return $this;
    }

    /**
    * Set or Get taxes array.
    *
    * @return object current instance.
    */
    public function taxes($taxes = null)
    {
        if (is_null($taxes)) {
            return $this->taxes;
        }

        $this->taxes = $taxes;
        return $this;
    }

    /**
    * Set or Get point sale.
    *
    * @return object current instance.
    */
    public function pointSale($pointSale = null)
    {
        if (is_null($pointSale)) {
            return $this->pointSale;
        }

        $this->pointSale = $pointSale;
        return $this;
    }

    /**
    * Set or Get document type.
    *
    * @return object current instance.
    */
    public function documentType($documentType = null)
    {
        if (is_null($documentType)) {
            return $this->documentType;
        }

        $this->documentType = $documentType;
        return $this;
    }

    /**
    * Set or Get document number.
    *
    * @return object current instance.
    */
    public function documentNumber($documentNumber = null)
    {
        if (is_null($documentNumber)) {
            return $this->documentNumber;
        }

        $this->documentNumber = $documentNumber;
        return $this;
    }

    /**
    * Set or Get date.
    *
    * @return object current instance.
    */
    public function date($date = null)
    {
        if (is_null($date)) {
            return $this->date;
        }

        $this->date = $date;
        return $this;
    }

    /**
    * Set or Get currency type.
    *
    * @return object current instance.
    */
    public function currencyType($currencyType = null)
    {
        if (is_null($currencyType)) {
            return $this->currencyType;
        }

        $this->currencyType = $currencyType;
        return $this;
    }

    /**
    * Set or Get currency trading.
    *
    * @return object current instance.
    */
    public function currencyTrading($currencyTrading = null)
    {
        if (is_null($currencyTrading)) {
            return $this->currencyTrading;
        }

        $this->currencyTrading = $currencyTrading;
        return $this;
    }

    /**
    * Set or Get from date.
    *
    * @return object current instance.
    */
    public function fromDate($fromDate = null)
    {
        if (is_null($fromDate)) {
            return $this->fromDate;
        }

        $this->fromDate = $fromDate;
        return $this;
    }

    /**
    * Set or Get to date.
    *
    * @return object current instance.
    */
    public function toDate($toDate = null)
    {
        if (is_null($toDate)) {
            return $this->toDate;
        }

        $this->toDate = $toDate;
        return $this;
    }

    /**
    * Set or Get expiration date.
    *
    * @return object current instance.
    */
    public function expirationDate($expirationDate = null)
    {
        if (is_null($expirationDate)) {
            return $this->expirationDate;
        }

        $this->expirationDate = $expirationDate;
        return $this;
    }

    /**
    * @todo refactor
    */
    public function getRequest($concepto)
    {
        $tiposIva=["21" => 5, "10.5" => 4];
        $ImpNet = 0;
        $ImpIVA = 0;

        foreach ($this->taxes() as $tipoIva => $datos) {
            $IVA['AlicIva'][] = ['Id' => $tiposIva[$tipoIva],
                                'BaseImp' => round($datos[0],2),
                                'Importe' => round($datos[1],2)];
            if (($this->voucherType()<11) || ($this->voucherType()>13)) {
              $ImpNet += $datos[0];
              $ImpIVA += $datos[1];
            } else {$ImpNet+=$datos[0]+$datos[1];}
        }

        $request = [
            'Concepto' => $concepto,
            'DocTipo' => $this->documentType(),
            'DocNro' => $this->documentNumber(),
            'CbteDesde' => $this->voucherNumber(),
            'CbteHasta' => $this->voucherNumber(),
            'CbteFch' => $this->date(),

            'ImpTotal' => round(0+$this->totalExempt()+$ImpNet+$ImpIVA+0,2), // ImpTotConc + ImpOpEx + ImpNeto + ImpIVA + ImpTrib
            'ImpTotConc' => 0,
            'ImpNeto' => round($ImpNet,2),
            'ImpOpEx'=> round($this->totalExempt(),2),
            'ImpIVA' => round($ImpIVA,2),
            'ImpTrib' => 0,

            'MonId'=> $this->currencyType(),
            'MonCotiz'=> $this->currencyTrading()
        ];

        if ($concepto>1) {
          $request['FchServDesde'] = $this->fromDate();
          $request['FchServHasta'] = $this->toDate();
          $request['FchVtoPago'] = $this->expirationDate();
        }

        if (($this->voucherType()<11) || ($this->voucherType()>13)) {$request['Iva']=$IVA;}

        $feCabReq = ['CantReg' => 1, 'PtoVta' => $this->pointSale(), 'CbteTipo' => $this->voucherType()];

        $feDetReq['FECAEDetRequest'][] = $request;

        $request['FeCAEReq'] = ['FeCabReq' => $feCabReq, 'FeDetReq' => $feDetReq];

        return $request;
    }
}
