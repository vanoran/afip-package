<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Production or Testing
    |--------------------------------------------------------------------------
    */
    'production' => false,

    /*
    |--------------------------------------------------------------------------
    | Testing Certificate file path
    |--------------------------------------------------------------------------
    */
    'certificate_file_dev' => "file://" . storage_path(env('DEV_AFIP_CERTIFICATION')),

    /*
    |--------------------------------------------------------------------------
    | Testing Private key file path
    |--------------------------------------------------------------------------
    */
    'private_key_dev' => 'file://' . storage_path(env('DEV_AFIP_PRIVATEKEY')),

    /*
    |--------------------------------------------------------------------------
    | Production Certificate file path
    |--------------------------------------------------------------------------
    */
    'certificate_file_pro' => "file://" . storage_path(env('PROD_AFIP_CERTIFICATION')),

    /*
    |--------------------------------------------------------------------------
    | Production Private key file path
    |--------------------------------------------------------------------------
    */
    'private_key_pro' => 'file://' . storage_path(env('PROD_AFIP_PRIVATEKEY')),

    /*
    |--------------------------------------------------------------------------
    | Configuration variables related to Production Environment
    |--------------------------------------------------------------------------
    */
    'wsaa_pro' => 'https://wsaa.afip.gov.ar/ws/services/LoginCms',
    'wsaa_wsdl_pro' => "https://wsaa.afip.gov.ar/ws/services/LoginCms?WSDL",
    'wsfe_pro' => "https://servicios1.afip.gov.ar/wsfev1/service.asmx",
    'wsfe_wsdl_pro' => "https://servicios1.afip.gov.ar/wsfev1/service.asmx?WSDL",

    /*
    |--------------------------------------------------------------------------
    | Configuration variables related to Testing Environment
    |--------------------------------------------------------------------------
    */
    'wsaa_dev' => 'https://wsaahomo.afip.gov.ar/ws/services/LoginCms',
    'wsaa_wsdl_dev' => "https://wsaahomo.afip.gov.ar/ws/services/LoginCms?WSDL",
    'wsfe_dev' => "https://wswhomo.afip.gov.ar/wsfev1/service.asmx",
    'wsfe_wsdl_dev' => "https://wswhomo.afip.gov.ar/wsfev1/service.asmx?WSDL",
];
